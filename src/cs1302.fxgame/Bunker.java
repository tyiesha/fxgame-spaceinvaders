package cs1302.fxgame;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 * Class to create the Bunker objects.
 * 
 * @author Tyiesha Holmes
 *
 */
public class Bunker extends Rectangle
{
	
	/**
	 * Constructor to set the width, height, color, and location
	 * of the Bunker objects.
	 * @param width
	 * @param height
	 * @param fill
	 * @param x
	 * @param y
	 */
	public Bunker(double width, double height, Paint fill, double x, double y)
	{
		super(width, height);
		setFill(fill);
		setTranslateX(x);
		setTranslateY(y);
	}//Bunker
	
}//Bunker
