package cs1302.fxgame;

import com.michaelcotterell.game.Game;
import com.michaelcotterell.game.GameTime;

import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class to create and run the SpaceInvaders game. 
 * 
 * @author Tyiesha Holmes
 *
 */
public class SpaceInvaders extends Game
{
	private int alienSpeed = 1;
	private int killCount = 0;
	private long score = 0;
	private int lives = 3; 
	private Boolean hit = false;
	private Boolean enemyHit = false;
	private double seconds = 5;
	private Random r = new Random();
	private int random = 0;
	private int mysteryShipTime = 0;
	int count = 0;
	private int bunkerCounter1 = 5;
	private int bunkerCounter2 = 5;
	private int bunkerCounter3 = 5;
	private int bunkerCounter4 = 5;
	private int bunkerCounter5 = 5;
	private int bunkerCounter6 = 5;
	private int bunkerCounter7 = 5;
	private int bunkerCounter8 = 5;
	private Boolean switchDirection = false;
	private Boolean moveDown = false;
	
	//cannon
	private Player cannon = new Player(40, 20, Color.YELLOW, 350, 500);
	
	//player bullet
	private Rectangle bullet = new Rectangle(4, 4) {{
		setFill(Color.WHITE);
	}};
	
	//enemy bullet
	private Rectangle enemyBullet = new Rectangle(4, 4) {{
		setFill(Color.WHITE);
	}};
	
	// rectangle to hold the background
    private Rectangle bg = new Rectangle(0, 0, 800, 550) {{ 
         setFill(Color.BLACK); 
    }};
    
    //score
    private Text totalScore = new Text("SCORE: " + Long.toString(score)) {{
    	setTranslateX(20);
    	setTranslateY(20);
    	setFill(Color.DARKORANGE);
    	
    }};

    //lives
    private Text totalLives = new Text("LIVES: " + Integer.toString(lives)) {{
    	setTranslateX(700);
    	setTranslateY(20);
    	setFill(Color.DARKORANGE);
    }};
	
	//mystery ship
	private Rectangle mysteryShip = new Rectangle(0, 0, 80, 30) {{
		setTranslateX(100);
		setTranslateY(100);
		setFill(Color.RED);
	}};
	
	//defense bunkers 1 row, 8 bunkers
	ArrayList<Bunker> bunkers = new ArrayList<Bunker>();
    private Bunker bunker1 = new Bunker(50, 20, Color.GREENYELLOW, 10, 450);
    private Bunker bunker2 = new Bunker(50, 20, Color.GREENYELLOW, 100, 450);
    private Bunker bunker3 = new Bunker(50, 20, Color.GREENYELLOW, 200, 450);
    private Bunker bunker4 = new Bunker(50, 20, Color.GREENYELLOW, 300, 450);
    private Bunker bunker5 = new Bunker(50, 20, Color.GREENYELLOW, 400, 450);
    private Bunker bunker6 = new Bunker(50, 20, Color.GREENYELLOW, 500, 450);
    private Bunker bunker7 = new Bunker(50, 20, Color.GREENYELLOW, 600, 450);
    private Bunker bunker8 = new Bunker(50, 20, Color.GREENYELLOW, 700, 450);
	
	//aliens 5 rows, 11 aliens
    ArrayList<Alien> aliens = new ArrayList<Alien>();
	private Alien alien1 = new Alien(10, 10, Color.BLUEVIOLET, 200, 240);
	private Alien alien2 = new Alien(10, 10, Color.BLUEVIOLET, 220, 240);
	private Alien alien3 = new Alien(10, 10, Color.BLUEVIOLET, 240, 240);
	private Alien alien4 = new Alien(10, 10, Color.BLUEVIOLET, 260, 240);
	private Alien alien5 = new Alien(10, 10, Color.BLUEVIOLET, 280, 240);
	private Alien alien6 = new Alien(10, 10, Color.BLUEVIOLET, 300, 240);
	private Alien alien7 = new Alien(10, 10, Color.BLUEVIOLET, 320, 240);
	private Alien alien8 = new Alien(10, 10, Color.BLUEVIOLET, 340, 240);
	private Alien alien9 = new Alien(10, 10, Color.BLUEVIOLET, 360, 240);
	private Alien alien10 = new Alien(10, 10, Color.BLUEVIOLET, 380, 240);
	private Alien alien11 = new Alien(10, 10, Color.BLUEVIOLET, 400, 240);
	private Alien alien12 = new Alien(10, 10, Color.BLUEVIOLET, 200, 220);
	private Alien alien13 = new Alien(10, 10, Color.BLUEVIOLET, 220, 220);
	private Alien alien14 = new Alien(10, 10, Color.BLUEVIOLET, 240, 220);
	private Alien alien15 = new Alien(10, 10, Color.BLUEVIOLET, 260, 220);
	private Alien alien16 = new Alien(10, 10, Color.BLUEVIOLET, 280, 220);
	private Alien alien17 = new Alien(10, 10, Color.BLUEVIOLET, 300, 220);
	private Alien alien18 = new Alien(10, 10, Color.BLUEVIOLET, 320, 220);
	private Alien alien19 = new Alien(10, 10, Color.BLUEVIOLET, 340, 220);
	private Alien alien20 = new Alien(10, 10, Color.BLUEVIOLET, 360, 220);
	private Alien alien21 = new Alien(10, 10, Color.BLUEVIOLET, 380, 220);
	private Alien alien22 = new Alien(10, 10, Color.BLUEVIOLET, 400, 220);
	private Alien alien23 = new Alien(10, 10, Color.BLUEVIOLET, 200, 200);
	private Alien alien24 = new Alien(10, 10, Color.BLUEVIOLET, 220, 200);
	private Alien alien25 = new Alien(10, 10, Color.BLUEVIOLET, 240, 200);
	private Alien alien26 = new Alien(10, 10, Color.BLUEVIOLET, 260, 200);
	private Alien alien27 = new Alien(10, 10, Color.BLUEVIOLET, 280, 200);
	private Alien alien28 = new Alien(10, 10, Color.BLUEVIOLET, 300, 200);
	private Alien alien29 = new Alien(10, 10, Color.BLUEVIOLET, 320, 200);
	private Alien alien30 = new Alien(10, 10, Color.BLUEVIOLET, 340, 200);
	private Alien alien31 = new Alien(10, 10, Color.BLUEVIOLET, 360, 200);
	private Alien alien32 = new Alien(10, 10, Color.BLUEVIOLET, 380, 200);
	private Alien alien33 = new Alien(10, 10, Color.BLUEVIOLET, 400, 200);
	private Alien alien34 = new Alien(10, 10, Color.BLUEVIOLET, 200, 180);
	private Alien alien35 = new Alien(10, 10, Color.BLUEVIOLET, 220, 180);
	private Alien alien36 = new Alien(10, 10, Color.BLUEVIOLET, 240, 180);
	private Alien alien37 = new Alien(10, 10, Color.BLUEVIOLET, 260, 180);
	private Alien alien38 = new Alien(10, 10, Color.BLUEVIOLET, 280, 180);
	private Alien alien39 = new Alien(10, 10, Color.BLUEVIOLET, 300, 180);
	private Alien alien40 = new Alien(10, 10, Color.BLUEVIOLET, 320, 180);
	private Alien alien41 = new Alien(10, 10, Color.BLUEVIOLET, 340, 180);
	private Alien alien42 = new Alien(10, 10, Color.BLUEVIOLET, 360, 180);
	private Alien alien43 = new Alien(10, 10, Color.BLUEVIOLET, 380, 180);
	private Alien alien44 = new Alien(10, 10, Color.BLUEVIOLET, 400, 180);
	private Alien alien45 = new Alien(10, 10, Color.BLUEVIOLET, 200, 160);
	private Alien alien46 = new Alien(10, 10, Color.BLUEVIOLET, 220, 160);
	private Alien alien47 = new Alien(10, 10, Color.BLUEVIOLET, 240, 160);
	private Alien alien48 = new Alien(10, 10, Color.BLUEVIOLET, 260, 160);
	private Alien alien49 = new Alien(10, 10, Color.BLUEVIOLET, 280, 160);
	private Alien alien50 = new Alien(10, 10, Color.BLUEVIOLET, 300, 160);
	private Alien alien51 = new Alien(10, 10, Color.BLUEVIOLET, 320, 160);
	private Alien alien52 = new Alien(10, 10, Color.BLUEVIOLET, 340, 160);
	private Alien alien53 = new Alien(10, 10, Color.BLUEVIOLET, 360, 160);
	private Alien alien54 = new Alien(10, 10, Color.BLUEVIOLET, 380, 160);
	private Alien alien55 = new Alien(10, 10, Color.BLUEVIOLET, 400, 160);
	
	/**
	 * Method to add aliens to game screen.
	 */
	public void addAliens()
	{
		aliens.add(alien1);
		aliens.add(alien2);
		aliens.add(alien3);
		aliens.add(alien4);
		aliens.add(alien5);
		aliens.add(alien6);
		aliens.add(alien7);
		aliens.add(alien8);
		aliens.add(alien9);
		aliens.add(alien10);
		aliens.add(alien11);
		aliens.add(alien12);
		aliens.add(alien13);
		aliens.add(alien14);
		aliens.add(alien15);
		aliens.add(alien16);
		aliens.add(alien17);
		aliens.add(alien18);
		aliens.add(alien19);
		aliens.add(alien20);
		aliens.add(alien21);
		aliens.add(alien22);
		aliens.add(alien23);
		aliens.add(alien24);
		aliens.add(alien25);
		aliens.add(alien26);
		aliens.add(alien27);
		aliens.add(alien28);
		aliens.add(alien29);
		aliens.add(alien30);
		aliens.add(alien31);
		aliens.add(alien32);
		aliens.add(alien33);
		aliens.add(alien34);
		aliens.add(alien35);
		aliens.add(alien36);
		aliens.add(alien37);
		aliens.add(alien38);
		aliens.add(alien39);
		aliens.add(alien40);
		aliens.add(alien41);
		aliens.add(alien42);
		aliens.add(alien43);
		aliens.add(alien44);
		aliens.add(alien45);
		aliens.add(alien46);
		aliens.add(alien47);
		aliens.add(alien48);
		aliens.add(alien49);
		aliens.add(alien50);
		aliens.add(alien51);
		aliens.add(alien52);
		aliens.add(alien53);
		aliens.add(alien54);
		aliens.add(alien55);
		
		for(int i = 0; i < aliens.size(); i++)
		{
			aliens.get(i).setVisible(true);
		}
	}//addAliens
	
	/**
	 * Constructor to add aliens, bunkers, cannon, totalScore, and other nodes to
	 * the scene.
	 * @param stage
	 */
	public SpaceInvaders(Stage stage)
	{
		super(stage, "Space Invaders 2.0", 60, 800, 550);
		enemyBullet.setVisible(false);
		
		getSceneNodes().getChildren().addAll(bg, totalScore, totalLives, cannon, bullet, enemyBullet);
		addAliens();
		getSceneNodes().getChildren().addAll(aliens);
		bunkers.add(bunker1);
		bunkers.add(bunker2);
		bunkers.add(bunker3);
		bunkers.add(bunker4);
		bunkers.add(bunker5);
		bunkers.add(bunker6);
		bunkers.add(bunker7);
		bunkers.add(bunker8);
		getSceneNodes().getChildren().addAll(bunkers);
	}//SpaceInvaders
	
	/**
	 * Update game loop. Runs the game. 
	 */
	@Override
	public void update(Game game, GameTime gameTime)
	{
		cannon.update(game, gameTime);
		//aliens.get(1).update(game, gameTime);
		//alien1.update(game, gameTime);
		
		
		//quit game
		if(game.getKeyManager().isKeyPressed(KeyCode.Q))
		{
			//game over quit screen instead
			System.out.println("You quit");
			
			System.exit(0);
		}
		
		
		//move aliens right and left   FIIXX to move them down when hit the edges
		//FIIXXX when they hit the right side, they get bunched up together
		for(int i = 0; i < aliens.size(); i++)
		{
			if(aliens.get(i).getTranslateX() >= game.getSceneBounds().getMaxX())
			{
				switchDirection = true;
				moveDown = true;
			}
			if(aliens.get(i).getTranslateX() <= game.getSceneBounds().getMinX())
			{
				switchDirection = false;
				moveDown = true;
			}
			
			if(switchDirection == true)
			{
				aliens.get(i).setTranslateX(aliens.get(i).getTranslateX() - alienSpeed);
				/*
				if(moveDown == true)
				{
					aliens.get(i).setTranslateY(aliens.get(i).getTranslateY() + 5);
					if(i == aliens.size()-1)
					{
						moveDown = false;
					}
					
				}
				*/
			}
			else
			{
				aliens.get(i).setTranslateX(aliens.get(i).getTranslateX() + alienSpeed);
				/*
				if(moveDown == true)
				{
					aliens.get(i).setTranslateY(aliens.get(i).getTranslateY() + 5);
					if(i == aliens.size()-1)
					{
						moveDown = false;
					}
					
				}
				*/
			}
			
		}
		
		//player shoots
        if(game.getKeyManager().isKeyPressed(KeyCode.SPACE))
        {
        	bullet.setVisible(true);
        	hit = true;
        	bullet.setTranslateX(cannon.getTranslateX()+15);
        	bullet.setTranslateY(cannon.getTranslateY());
        }
    	bullet.setTranslateY(bullet.getTranslateY() - 5);
    	
    	//FIXX bullet 'disappears' when alien speed too fast
    	//alien random shooting
        if(gameTime.getTotalGameTime().getSeconds() == seconds)
        {
        	enemyBullet.setVisible(true);
        	random = r.nextInt(aliens.size());
        	enemyHit = true;
        	
        	enemyBullet.setTranslateX(aliens.get(random).getTranslateX());
        	enemyBullet.setTranslateY(aliens.get(random).getTranslateY());
        	
        	seconds += 10;
        }
        enemyBullet.setTranslateY(enemyBullet.getTranslateY() + 5);
    	
    	//hit bunker, takes damage  try after 5 hits, it disappears
        for(int i = 0; i < bunkers.size(); i++)
        {
        	if(bullet.getBoundsInParent().intersects(bunkers.get(i).getBoundsInParent()))
            {
            	/*
        		if(bunkers.get(i).equals(bunker1))
            	{
            		bunkerCounter1--;
            		if(bunkerCounter1 == 0)
            		{
            			bunkers.get(i).setVisible(false);
            			bunkers.remove(i);
            		}
            	}
            	if(bunkers.get(i).equals(bunker2))
            	{
            		bunkerCounter2--;
            	}
            	if(bunkers.get(i).equals(bunker3))
            	{
            		bunkerCounter3--;
            	}
            	if(bunkers.get(i).equals(bunker4))
            	{
            		bunkerCounter4--;
            	}
            	if(bunkers.get(i).equals(bunker5))
            	{
            		bunkerCounter5--;
            	}
            	if(bunkers.get(i).equals(bunker6))
            	{
            		bunkerCounter6--;
            	}
            	if(bunkers.get(i).equals(bunker7))
            	{
            		bunkerCounter7--;
            	}
            	if(bunkers.get(i).equals(bunker8))
            	{
            		bunkerCounter8--;
            	}
            	*/
        		if(hit == true)
            	{	
            		hit = false;
            		bullet.setVisible(false);
            		
            		
            		/*if(bunkerCounter2 == 0){
            			bunkers.get(1).setVisible(false);
            			bunkers.remove(1);
            		}*/
            		
            		//SHOULD REMOVE BUNKER AFTER DAMAGE ALOT
            		bunkers.get(i).setVisible(false);
            		bunkers.remove(i);
            		
            		return;
            	}
            	
            }
        	
            if(enemyBullet.getBoundsInParent().intersects(bunkers.get(i).getBoundsInParent()))
            {
            	if(enemyHit == true)
            	{
            		enemyHit = false;
            		enemyBullet.setVisible(false);
            		
            		//make sure same as ^^^^ [player bullet]
            		bunkers.get(i).setVisible(false);
            		bunkers.remove(i);
            		
            		return;
            	}
            }
        }
        
        //deal with aliens being hit
        for(int i = 0; i < aliens.size(); i++)
        {
        	if(bullet.getBoundsInParent().intersects(aliens.get(i).getBoundsInParent()))
        	{
        		if(hit == true)
        		{
        			score += 10;
        			totalScore.setText("SCORE: " + Long.toString(score));
        			hit = false;
        			
        			aliens.get(i).setVisible(false);
        			aliens.remove(i);
        			bullet.setVisible(false);
        			killCount++;
        			if(killCount == 8)
        			{
        				alienSpeed++;
        				killCount = 0;
        			}
        			return;
        		}
        	}
        }
        
        //if cannon hit, lose a life, respawn to starting position
        if(enemyBullet.getBoundsInParent().intersects(cannon.getBoundsInParent()))
        {
        	if(enemyHit == true)
        	{
        		lives -= 1;
        		totalLives.setText("LIVES: " + Integer.toString(lives));
        		enemyHit = false;
        		enemyBullet.setVisible(false);
        		cannon.setTranslateX(350);
        		cannon.setTranslateY(500);
        	}
        }
        
        
        
        //add mystery ship at random and make it disappear (100 points if hit)
        /*if(bullet.getBoundsInParent().intersects(mysteryShip.getBoundsInParent()))
        {
        	if(hit == true)
        	{
        		score += 100;
        		totalScore.setText("SCORE: " + Long.toString(score));
        		hit = false;
        		
        	}
        }*/
        /*
        if(gameTime.getTotalGameTime().getSeconds() == 10)
        {
        	mysteryShip.setVisible(true);
        	
        	mysteryShipTime += 10;
        }
        mysteryShip.setTranslateX(mysteryShip.getTranslateX() + 3);
        */
        
        
        
        //player wins    respawn aliens, maybe pause game for a few seconds
        //FIIXXX removeall and addall wont work because loop runs multiple
        //times per second
        if(aliens.size() == 0)
        {
        	//delete this later
        	System.out.println("YOU WIN");
        	/*getSceneNodes().getChildren().removeAll(aliens);
        	addAliens();
        	getSceneNodes().getChildren().addAll(aliens);*/
        	game.stop();
        }
        
        //player loses   maybe have lose animation
        if(lives == 0)
        {
        	//delete sysout
        	System.out.println("GAME OVER");
        	
        	game.stop();
        	
        }
		
	}//update

	
	
}//SpaceInvaders
