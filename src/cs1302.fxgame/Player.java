package cs1302.fxgame;

import com.michaelcotterell.game.Game;
import com.michaelcotterell.game.GameTime;
import com.michaelcotterell.game.Updateable;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 * Class to create the Player cannon object.
 * 
 * @author Tyiesha Holmes
 *
 */
public class Player extends Rectangle implements Updateable
{
	protected double movement = 6;
	
	/**
	 * Constructor to set the width, height, color, and starting
	 * location of the player cannon object.
	 * @param width
	 * @param height
	 * @param fill
	 * @param x
	 * @param y
	 */
	public Player(double width, double height, Paint fill, double x, double y)
	{
		super(width, height);
		setFill(fill);
		setTranslateX(x);
		setTranslateY(y);
	}//Player
	
	/**
	 * Method to control the player's movements based on which key is pressed.
	 * Checks the game screen bounds to prevent the player from going out of view. 
	 * Called from the SpaceInvaders update method.
	 */
	@Override
	public void update(Game game, GameTime gameTime)
	{
		double dx = 0;
		
		if(game.getKeyManager().isKeyPressed(KeyCode.RIGHT))
		{
			dx += movement;
		}
		if(game.getKeyManager().isKeyPressed(KeyCode.LEFT))
		{
			dx -= movement;
		}
		
		//check right
		if(getTranslateX() + getWidth() + dx >= game.getSceneBounds().getMaxX())
		{
			dx = game.getSceneBounds().getMaxX() - getBoundsInParent().getMaxX();
		}
		//check to left
		if(getTranslateX() + dx <= game.getSceneBounds().getMinX())
		{
			dx = getBoundsInParent().getMinX() - game.getSceneBounds().getMinX();
		}
		
		//move player
		setTranslateX(translateXProperty().add(dx).get());
		
	}//update
	
}//Player
