package cs1302.fxgame;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 * Class to create Alien objects
 * 
 * @author Tyiesha Holmes
 *
 */
public class Alien extends Rectangle
{
	
	/**
	 * Constructor to set the height, width, color, and location of the
	 * Alien object.
	 * @param width
	 * @param height
	 * @param fill
	 * @param x
	 * @param y
	 */
	public Alien(double width, double height, Paint fill, double x, double y)
	{
		super(width, height);
		setFill(fill);
		setTranslateX(x);
		setTranslateY(y);
	}//Alien
	
	//TRY TO MOVE ALIEN MOVE STUFF HERE
		
}//Alien
