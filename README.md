# Project Submission (CS1302)

Author: TYIESHA HOLMES

# Gameplay Instructions

Use 'Spacebar' to shoot a bullet from the player cannon at the bottom. Use the 'left' or 'right' arrow buttons to move the cannon. To quit the game, press 'q' and the screen will exit. The player has 3 lives. If the player loses all 3 lives, the game exits. The aliens shoot their bullets every 10 seconds. The player gets 10 points for each alien they hit. For every 8 aliens killed, the speeds for the increases. If the player or the aliens hit a defense bunker, it disappears.

# NOTES 
need to do:

-intro
-game over screen
-mysteryship display
-aliens move down a row
-bunker hit counters